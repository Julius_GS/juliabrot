using DelimitedFiles, ImageView, Images

function iteration(c, ceil, maxit)
    z = 0
    j = 1
    while (j <= maxit && abs(z) < ceil)
        z = z^2+c
        j += 1
    end
    if (j == maxit) j = 0 end
    return j
end

function gridinit(center, pixel, step, ceil, maxit)
    A = Array{UInt}(undef,(2*pixel[1]+1,2*pixel[2]+1))
    c = center - pixel[1]*step - pixel[2]*step*im
    dummy = real(c)
    for i = 1 : size(A)[2]
        for j = 1 : size(A)[1]
            c += step
            A[j,i] = iteration(c,ceil, maxit)
        end
        c = (imag(c)+step)*im+dummy
    end
    return transpose(A./(maxit+1));
end

function main(center, pixel, step, ceil, maxit)
    A = gridinit(center, pixel, step, ceil, maxit);
    ImageView.imshow(A)
    save("fig.png", colorview(Gray, A));
    return A
end
